**docker django 2021/11/20 - Pieter van der Merwe**
https://www.youtube.com/watch?v=mScd-Pc_pX0

#TESTING
using circle CI for integration testing. to test
```
git checkout TESTING
git merge master
git push
```
see https://app.circleci.com/pipelines/bitbucket/PieterThunderMerwe/django-202111?branch=TESTING&filter=all


# *Useful* wrap all your standar python/django commands in this docker-compuse run
```
docker-compose run --rm app sh -c ""
```

## SETUP

1. goto projects dir
```
cd /e/Work/enlabeler/
```
2. clone repo
```
git clone https://PieterThunderMerwe@bitbucket.org/PieterThunderMerwe/django-202111.git
cd django-202111
```
3. create folders & files, ./app, .dockerignore, docker-compose.yml, Dockerfile, requirements.txt
```
mkdir app
touch .dockerignore docker-compose.yml Dockerfile requirements.txt
```
*NB! last line in docker file is changing the user from root to app. this is good for security, but makes adding new packages difficult via ```d-c run``` operations. (Errno:13 Permission denied)

4. build docker img (name: django-202111_app)
```
docker-compose build
```
5. ini Django Application "app"
```
docker-compose run --rm app sh -c "django-admin startproject app ."
```
6. tune app/app/settings.py
```
import os
SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = bool(int(os.environ.get('DEBUG',0)))
ALLOWED_HOSTS = [] #!debug
ALLOWED_HOSTS.extend(filter(
    None,
    os.environ.get('ALLOWED_HOSTS','').split(',')
))
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.environ.get('DB_HOST'),
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASS'),
    }
}
```

7. create Django model "core" app
```
docker-compose build
docker-compose run --rm app sh -c "python manage.py startapp core"

app/app/settings.py <
INSTALLED_APPS += 'core'

app/core/models.py < define new Sample class
app/core/admin.py < register new class

docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
```

8. wait_for_db.py

9. docker up
```
docker-compose up
```

working running app
---

---
##MISC
---

update models
```
python manage.py makemigrations
python manage.py migrate
```


file upload app, see file-upload-app* in repo
```
changes to dockerfile >> docker-compose build
docker-compose up
docker-compose down
** change dockerfile to volume the db (use vscode terminal for TTY ops)
docker-compose run --rm app sh -c "python manage.py createsuperuser"
docker-compose up
```


###build proxy
```
mkdir ./proxy
```

```
cat <<UWSGI_PARAMS >> ./proxy/uwsgi_params
uwsgi_param QUERY_STRING \$query_string;
uwsgi_param REQUEST_METHOD \$request_method;
uwsgi_param CONTENT_TYPE \$content_type;
uwsgi_param CONTENT_LENGTH \$content_length;
uwsgi_param REQUEST_URI \$request_uri;
uwsgi_param PATH_INFO \$document_uri;
uwsgi_param DOCUMENT_ROOT \$document_root;
uwsgi_param SERVER_PROTOCOL \$server_protocol;
uwsgi_param REMOTE_ADDR \$remote_addr;
uwsgi_param REMOTE_PORT \$remote_port;
uwsgi_param SERVER_ADDR \$server_addr;
uwsgi_param SERVER_PORT \$server_port;
uwsgi_param SERVER_NAME \$server_name;
UWSGI_PARAMS
```

```
cat <<DEFAULT_CONF_TPL >>  ./proxy/default.conf.tpl
server {
   listen \${LISTEN_PORT};

   location /static {
      alias /vol.static;
   }

   location / {
      uwsgi_pass              \${APP_HOST}:\${APP_PORT};
      include                 /etc/nginx/uwsgi_params;
      client_max_body_size    100M;
   }
}
DEFAULT_CONF_TPL
```

```
cat <<RUN_SH >>  ./proxy/run.sh
#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'
RUN_SH
```

```
touch ./proxy/Dockerfile
touch ./docker-compose-prod.yml
```
# build prod docker
```
docker-compose down --volumes
docker-compose -f docker-compose-prod.yml build
docker-compose -f docker-compose-prod.yml up

docker-compose -f docker-compose-prod.yml down #to switch deteween prod & dev env
```




# Rest API (https://www.youtube.com/watch?v=RPsDhoWY_kc)
# https://www.django-rest-framework.org/
# https://www.django-rest-framework.org/api-guide/schemas/
#restapi
```
docker-compose run --rm app sh -c "pip install djangorestframework"
docker-compose run --rm app sh -c "pip install markdown" # Markdown support for the browsable API.
docker-compose run --rm app sh -c "pip install django-filter"
```

Test with:
http://localhost:8000/api/test
http://localhost:8000/users/
http://localhost:8000/api-auth/users/
http://localhost:8000/api-auth/clients/

docker-compose run --rm app sh -c "python -m pip install pyyaml"
docker-compose run --rm app sh -c "pip install uritemplate"
#NOTE this didnt' work for me, had to add pyyaml and uritemplate to the requirements.txt, down & rebuild the image, up
docker-compose run --rm app sh -c "./manage.py generateschema --file openapi-schema.yml"



#jquery
```
docker-compose run --rm app sh -c "pip install django-static-jquery==2.1.4"
docker-compose run --rm app sh -c "pip install django-input-mask"
```



#testing & coverage
(https://coverage.readthedocs.io/en/6.2/#quick-start)
this doesnt work in docker so console in and run on the vhost (https://stackoverflow.com/questions/48692363/coverage-run-app-py-inside-a-docker-container-not-creating-coverage-file)
()
```
pip install coverage==6.2
coverage run manage.py test -v 2
coverage report -m
coverage html
run file:///E:/Work/enlabeler/django-202111/app/htmlcov/index.html

```
[![CircleCI](https://circleci.com/bb/PieterThunderMerwe/django-202111/tree/TESTING.svg?style=svg)](https://circleci.com/bb/PieterThunderMerwe/django-202111/tree/TESTING)


# ???
```
docker-compose run --rm app sh -c " ??? "
```
---