FROM python:3.9-alpine3.13
LABEL maintainer="https://bitbucket.org/PieterThunderMerwe"

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
COPY ./app /app
COPY ./scripts /scripts
#src code

WORKDIR /app
EXPOSE 8000

RUN python -m venv /py && \
	/py/bin/pip install --upgrade pip && \
	apk add --update --no-cache postgresql-client && \
	apk add --update --no-cache --virtual .tmp-deps \
		build-base postgresql-dev musl-dev linux-headers && \
	/py/bin/pip install -r /requirements.txt && \
	apk del .tmp-deps && \
	adduser --disabled-password --no-create-home app && \
	mkdir -p /vol/web/static && \
	mkdir -p /vol/web/media && \
	chown -R app:app /vol && \
	chmod -R 755 /vol && \
	chmod -R +x /scripts



#requirements.txt
# psycopg2>=2.8.6,<2.9 == required dependancies build-base postgresql-dev musl-dev

ENV PATH="/scripts:/py/bin:$PATH"

# USER app #TODO: reenable on prod (causes Errno:13 Permission denied)

CMD ["run.sh"]

# onchange run: docker-compose build