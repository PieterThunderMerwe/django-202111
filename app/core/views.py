from django.http import JsonResponse
from django.shortcuts import render
import app as App
import sys

#3rd party
from rest_framework import routers, serializers, viewsets, schemas #restapi
from rest_framework.permissions import *
from rest_framework.response import Response
from rest_framework.views import APIView #restapi


### Create your views here. ###

#API:Client
from .serializers import ClientSerializer
from .models import Client
class ClientViewSet(viewsets.ModelViewSet):
	queryset = Client.objects.filter(Active=True)
	serializer_class = ClientSerializer
#TEST: http://localhost:8000/api-auth/clients/

#API:Language
from .serializers import LanguageSerializer
from .models import Language
class LanguageViewSet(viewsets.ModelViewSet):
	queryset = Language.objects.filter(Active=True)
	serializer_class = LanguageSerializer
#TEST: http://localhost:8000/api-auth/languages/

#API:UserLanguageRating
from .serializers import UserLanguageRatingSerializer
from .models import UserLanguageRating
class UserLanguageRatingViewSet(viewsets.ModelViewSet):
	queryset = UserLanguageRating.objects.filter(Active=True)
	serializer_class = UserLanguageRatingSerializer
#TEST: http://localhost:8000/api-auth/languages/


## TESTING !! REMOVE LATER
#OpenAPI
# class OpenAPIView(APIView):
	# permission_classes = [AllowAny]

	# @classmethod
	# def get_extra_actions(cls):
	# 	return []

	# def get(self, request, format=None):
	# 	s = schemas.get_schema_view(
	# 		title=App.__name__ ,
	# 		description="OpenApi Schema",
	# 		version=App.__build__
	# 	)
	# 	sys.stdout.write(s.)
	# 	data = {
	# 		"name": "pieter's"
	# 		, "email": "pieter.thunder.merwe@gmail.com"
	# 	}

	# 	return Response(data)

	# def get_object(self, queryset=None):
	# 	if queryset is None:
	# 		queryset = self.get_queryset()

	# 	return queryset.get(key=self.kwargs["key"].lower())

	# 	try:
	# 		return queryset.get(key=self.kwargs["key"].lower())
	# 	except Client.DoesNotExist:
	# 		return None

	# def get_queryset(self):
	# 	return None
#TEST: http://localhost:8000/openapi/

# def test_view(requrest):

# 	data = {
# 		"name": "pieter's"
# 		, "email": "pieter.thunder.merwe@gmail.com"
# 	}

# 	return JsonResponse(data, safe=False)
#TEST: http://localhost:8000/api/test (see app.urls.py)

class TestView(APIView):
	def get(self, request, *args, **kwargs):
		data = {
			"name": "pieter's"
			, "email": "pieter.thunder.merwe@gmail.com"
		}

		return Response(data)
# TEST: http://localhost:8000/api/test (see app.urls.py)


#restapi: simple rest interface for users model (http://localhost:8000/api-auth/users/)
# Serializers define the API representation.
# class UserSerializer(serializers.HyperlinkedModelSerializer):
# 	class Meta:
# 		model = User
# 		fields = ['url', 'username', 'email', 'is_staff','last_login']
# 		#exclude = ['username']
# #TODO: move to core.serilizers.py - done


from .serializers import UserSerializer
from django.contrib.auth.models import User
class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer
# router.register(r'api-auth/users', UserViewSet) in app.urls.py
#TEST: http://localhost:8000/api-auth/users/


from .serializers import GeeksSerializer
from .models import GeeksModel
class GeeksViewSet(viewsets.ModelViewSet):
	queryset = GeeksModel.objects.all()
	serializer_class = GeeksSerializer
#TEST: http://localhost:8000/api-auth/geeks/