# Generated by Django 3.2.11 on 2022-01-05 19:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20220105_2115'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='Notes',
            field=models.TextField(blank=True, help_text='NB! For internal use only!'),
        ),
    ]
