# Generated by Django 3.2.10 on 2022-01-05 15:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_remove_client_clientcode'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='ClientCode',
            field=models.CharField(default='EN0000', help_text='ABC123', max_length=6, unique=True, verbose_name='Client Code'),
            preserve_default=False,
        ),
    ]
