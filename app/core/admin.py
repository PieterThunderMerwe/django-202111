from django.contrib import admin
from core.models import *


# Register your models here. #NB, not all need to be registered in the django/admin site
admin.site.register(Client, ClientAdmin)
admin.site.register(Language, LanguageAdmin)
admin.site.register(UserLanguageRating, UserLanguageRatingAdmin)

## TESTING !! REMOVE LATER
# admin.site.register(Sample)
# admin.site.register(GeeksModel)
