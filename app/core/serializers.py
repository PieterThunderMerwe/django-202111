from rest_framework import serializers

#Client
from .models import Client
class ClientSerializer(serializers.HyperlinkedModelSerializer):
	# specify model and fields
	class Meta:
		model = Client
		#fields = ('ClientName', 'ClientCode','Logo','ContactEmail','ApiKey','ApiDeliveryEndpoint','Notes','Active','FirstEdit','LastEdit')
		exclude = ('ApiKey','Notes',) #SECRETS


#Language
from .models import Language
class LanguageSerializer(serializers.HyperlinkedModelSerializer):
	# specify model and fields
	class Meta:
		model = Language
		#fields = ('LanguageName', 'LanguageCode')
		exclude = ('Notes',)


#UserLanguageRating
from .models import UserLanguageRating
class UserLanguageRatingSerializer(serializers.ModelSerializer):
	# specify model and fields
	class Meta:
		model = UserLanguageRating
		#fields = ('Level')
		exclude = ('Notes',)


## TESTING !! REMOVE LATER
from .models import GeeksModel
class GeeksSerializer(serializers.HyperlinkedModelSerializer):
	# specify model and fields
	class Meta:
		model = GeeksModel
		fields = ('url', 'id', 'title', 'description')
#TEST: http://localhost:8000/api-auth/geeks/


from django.contrib.auth.models import User
class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ['id','username','first_name','last_name','email','is_staff','is_active','is_superuser','date_joined','last_login']
		#exclude = ('password',)
#TEST: http://localhost:8000/api-auth/users/