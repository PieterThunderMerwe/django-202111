"""
Django command to wait for the database to be available.
"""
import sys, time

from django.core.management.base import BaseCommand
from django.db.utils import OperationalError

from psycopg2 import OperationalError as Psycopg2OpError

class Command(BaseCommand):
	"""Django command to wait for database. run as: cd app
		python manage.py wait_for_db
		docker-compose run --rm app sh -c "python manage.py wait_for_db"
	"""

	def handle(self, *args, **options):
		"""Entrypoint for command."""
		self.stdout.write('Waiting for database...')
		db_up = False
		i = 0

		while db_up is False:
			try:
				self.check(databases=['default'])
				db_up = True
			except (Psycopg2OpError, OperationalError):
				if i > 3:
					self.stderr.write(self.style.ERROR('Database unavailable!'))
					self.exitcode = 1
					return

				self.stdout.write('Database unavailable, waiting 1 second...')
				time.sleep(2)
				i+=1

		self.stdout.write(self.style.SUCCESS('Database available!'))
		self.exitcode = 0
		return