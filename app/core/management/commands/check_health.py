"""
Django command to wait for the database to be available.
"""
import sys, time

from django.core.management.base import BaseCommand
from django.db.utils import OperationalError

class Command(BaseCommand):
	"""Collect system health metrics. run as: cd app
		python app/manage.py check_health #this should fail (no db)
		docker-compose run --rm app sh -c "python manage.py check_health" #this should pass
	"""

	def handle(self, *args, **options):
		#ini
		# self.stdout.write('HealthCheck ini...')

		try:
			self.check(databases=['default'])
		except (OperationalError):
			self.stderr.write(self.style.ERROR('HealthCheck: Failed'))
			self.exitcode = 1
			return

		#post
		self.stdout.write(self.style.SUCCESS('HealthCheck: Passed'))
		self.exitcode = 0
		return