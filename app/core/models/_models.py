#process: adding new model to applications (MVC)
"""
M) define model in core/models.py
V) register models in core/admin.py || define view in core/views.py
C) add route to app/urls.py

"""
#OnChange run:
"""
docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
"""

from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.timezone import datetime
from core.utils import *
# import logging
# import time


#from django.conf import settings



# Create your models here.
##CLASS _________(models.Model):
"""A typical class defining a model, derived from the Model class."""
	# Fields
	# my_field_name = models.CharField(max_length=20, help_text='Enter field documentation')

	# Metadata
	# class Meta:
	# 	ordering = ['-my_field_name']

	# Methods
##eoCLASS

##CLASS EnBaseModel (Adstract)
class EnBaseModel(models.Model):
	""" BUSINESS RULES:

	"""
	# Fields
	Notes = models.TextField(blank = True, help_text='NB! For internal use only!')
	Active = models.BooleanField(default=True)
	#FirstUser = models.CharField(max_length = 48, editable=False, default='SYSTEM', verbose_name='Created By') #nice to have, but django admin tracks history in django_admin_log table
	FirstEdit = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Created At')
	#LastUser = models.CharField(max_length = 48, editable=False, default='SYSTEM', verbose_name='Updated By')
	LastEdit = models.DateTimeField(auto_now=True, editable=False, verbose_name='Last Updated At')

	list_display = ['Active','LastEdit']
	list_filter = ['Active']
	fields = ('Notes','Active') #exclude non-editable fields and add to read-only for display [,'FirstEdit','LastEdit']
	readonly_fields = ['FirstEdit','LastEdit']

	# Metadata
	class Meta:
		abstract = True
		indexes = [
			models.Index(fields=['Active']),
		]

	# Methods
##eoCLASS EnBaseModel (Adstract)



## TESTING !! REMOVE LATER
class Sample(models.Model):
	attachment = models.FileField()
	title = models.TextField(default='')
	url = models.TextField(default='')


class GeeksModel(models.Model):
	title = models.CharField(max_length = 200)
	description = models.TextField()

	def __str__(self):
		return self.title
