#process: adding new model to applications (MVC)
"""
M) define model in core/models.py
V) register models in core/admin.py || define view in core/views.py
C) add route to app/urls.py

"""
#OnChange run:
"""
docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
"""

from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.html import format_html
from django.utils.timezone import datetime
from core.utils import *
from ._models import EnBaseModel

# import logging
# import time
# inport sys

#from django.conf import settings


##CLASS Language
class Language(EnBaseModel):
	""" BUSINESS RULES: Lang.
		see list of iso codes https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
	"""
	# Attr
	LanguageCode_max_length = 9 #lang + region [+ dialect], e.g. en-za-cpt could repesent cape-afrikaans
	LanguageCode_min_length = 5

	# Fields
	LanguageName = models.CharField(max_length = 200, help_text='Language\Region\Dialect, e.g. "English\South Africa"', unique=True, verbose_name='Name')
	LanguageCode = models.CharField(max_length = LanguageCode_max_length, help_text='en-za. see ISO 3166-1', unique=True, verbose_name='ISO Code')


	# Metadata
	class Meta:
		indexes = [
			models.Index(fields=['LanguageName']),
			models.Index(fields=['LanguageCode']),
		]
		ordering = ['LanguageName']

	# Methods
	def __str__(self):
		return self.LanguageName + " [" + self.LanguageCode +"]"


class LanguageForm(forms.ModelForm):
	class Meta:
		model = Language
		fields = ('LanguageName', 'LanguageCode') + EnBaseModel.fields
		# widgets = {'LanguageCode': forms.TextInput(attrs={'data-mask':"##-##-###"})} #prints to html, nut jquery doesn't fire

	def clean(self):
		#validate APIKey = 32cahrs
		LanguageCode = self.cleaned_data.get('LanguageCode')
		if not LanguageCode \
			or len(LanguageCode) < Language.LanguageCode_min_length \
			or len(LanguageCode) > Language.LanguageCode_max_length:
				message = format_html("Language ISO Code format incorrect. Format as Language-Region-Dialect codes where Language=<a href='{}' target=_blank>ISO-639-1</a> \
				, Region=<a href='{}' target=_blank>ISO 3166-1-A2</a> \
				, Dialect=<a href='{}' target=_blank>ISO-639-3</a> (optional)."
					, "https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes"
					, "https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes"
					, "https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes")
				raise forms.ValidationError(message)
		return self.cleaned_data

class LanguageAdmin(admin.ModelAdmin):
	# list
	list_display = ['LanguageName', 'LanguageCode'] + EnBaseModel.list_display
	search_fields = ['LanguageName', 'LanguageCode','Notes']
	list_filter = EnBaseModel.list_filter
	# details
	form = LanguageForm
	readonly_fields = EnBaseModel.readonly_fields


##eoCLASS Language