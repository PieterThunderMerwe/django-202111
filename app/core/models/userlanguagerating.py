#process: adding new model to applications (MVC)
"""
M) define model in core/models.py
V) register models in core/admin.py || define view in core/views.py
C) add route to app/urls.py

"""
#OnChange run:
"""
docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
"""

from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.html import format_html
from django.utils.timezone import datetime
from .language import Language
from django.contrib.auth.models import User
from core.utils import *

from ._models import EnBaseModel

# import logging
# import time
# inport sys

#from django.conf import settings


##CLASS UserLanguageRating
class UserLanguageRating(EnBaseModel):
#class UserLanguageRating(models.Model):
	""" BUSINESS RULES: UserLanguageRating - control

	"""
	# Attr
	class Levels(models.TextChoices):
		Basic = 'Basic', "Basic"
		Advance = 'Advance', "Advance"
		Expert = 'Expert', "Expert"

	# Fields
	Language = models.ForeignKey(Language, on_delete=models.DO_NOTHING, verbose_name='Language')
	User = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name='User')
	Level = models.CharField(max_length=20, choices=Levels.choices, default=Levels.Basic)


	# Metadata
	class Meta:
		indexes = []
		ordering = ['-LastEdit']

	# Methods
	def __str__(self):
		return str(self.Level) + " " + str(self.Language) + " - "+ str(self.User)


class UserLanguageRatingForm(forms.ModelForm):
	class Meta:
		model = UserLanguageRating
		fields = ('Level','Language','User') + EnBaseModel.fields

class UserLanguageRatingAdmin(admin.ModelAdmin):
	# list
	list_display = ['Level','Language','User'] + EnBaseModel.list_display
	search_fields = ['Notes','Language__LanguageName','User__username'] # use User.username
	list_filter = ['Level','Language','User'] + EnBaseModel.list_filter
	# details
	form = UserLanguageRatingForm
	readonly_fields = EnBaseModel.readonly_fields


##eoCLASS UserLanguageRating