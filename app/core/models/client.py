#process: adding new model to applications (MVC)
"""
M) define model in core/models.py
V) register models in core/admin.py || define view in core/views.py
C) add route to app/urls.py

"""
#OnChange run:
"""
docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
"""

from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.timezone import datetime
from core.utils import *
from ._models import EnBaseModel

# import logging
# import time
# inport sys

#from django.conf import settings


##CLASS Client
class Client(EnBaseModel):
	""" BUSINESS RULES:

	"""
	#attr
	ApiKey_max_length = 32

	# Fields
	ClientName = models.CharField(max_length = 200, verbose_name='Name')
	ClientCode = models.CharField(max_length = 6, help_text='ABC123', unique=True, verbose_name='Code')
	Logo = models.FileField(null=True, blank=True)
	ContactEmail = models.EmailField(max_length = 200, verbose_name='Contact Email')

	ApiKey = models.CharField(max_length = ApiKey_max_length, null=True, blank=True, verbose_name='API Key')
	ApiDeliveryEndpoint = models.URLField(max_length = 200, null=True, blank=True, verbose_name='API Delivery Endpoint')
	# Notes = EnBaseModel.fields['Notes'] #inherit from EnBaseModel


	# Metadata
	class Meta:
		indexes = [
			models.Index(fields=['ClientName']),
			models.Index(fields=['ClientCode']),
			models.Index(fields=['ApiKey']),
		]
		ordering = ['ClientName']
		# constraints = [ #wip
		# 	models.CheckConstraint(
		# 		check=models.Q(ClientCode__length__gte=6),
		# 		name="ClientCode_min_length",
		# 	)
		# ]

	# Methods
	def __str__(self):
		return self.ClientName + " [" + self.ClientCode +"]"

	def getApiKeyLength(self):
		if self.ApiKey:
			return len(self.ApiKey)
		else:
			return -1


class ClientForm(forms.ModelForm):
	class Meta:
		model = Client
		fields = ('ClientName', 'ClientCode','Logo','ContactEmail','ApiKey','ApiDeliveryEndpoint') + EnBaseModel.fields #reorder fields

	def clean(self):
		#validate APIKey = 32cahrs
		ApiKey = self.cleaned_data.get('ApiKey')
		if not ApiKey or len(ApiKey) != Client.ApiKey_max_length:
			raise forms.ValidationError("Client ApiKey invalid (len:32chars)")
		return self.cleaned_data

class ClientAdmin(admin.ModelAdmin):
	# list
	list_display = ['ClientName', 'ClientCode'] + EnBaseModel.list_display
	search_fields = ['ClientName', 'ClientCode','ContactEmail','ApiKey','ApiDeliveryEndpoint','Notes']
	list_filter = EnBaseModel.list_filter
	# details
	form = ClientForm
	readonly_fields = EnBaseModel.readonly_fields

##eoCLASS Client