from django.core.management import call_command
from django.test import TestCase
from io import StringIO
from unittest import mock
import warnings, sys


#OnChange run:
"""
docker-compose run --rm app sh -c "python -Wa manage.py test"
docker-compose run --rm app sh -c "python -Wa manage.py test --keepdb"
"""

class EnTestCase(TestCase):

	@classmethod
	def build_argv(cls, *positional, short_names=None, long_names=None, short_dict=None, **long_dict):
		"""
		Build argv list which can be provided for manage command "run_from_argv"
		1) positional will be passed first as is
		2) short_names with be passed after with one dash (-) prefix
		3) long_names with be passed after with one tow dashes (--) prefix
		4) short_dict with be passed after with one dash (-) prefix key and next item as value
		5) long_dict with be passed after with two dashes (--) prefix key and next item as value
		"""
		argv = [__file__, None] + list(positional)[:]

		for name in short_names or []:
			argv.append(f'-{name}')

		for name in long_names or []:
			argv.append(f'--{name}')

		for name, value in (short_dict or {}).items():
			argv.append(f'-{name}')
			argv.append(str(value))

		for name, value in long_dict.items():
			argv.append(f'--{name}')
			argv.append(str(value))

		return argv

	@classmethod
	def run_manage_command_raw(cls, cmd_class, argv):
		"""
		run any manage.py command as python object.
		use by running
			argv = self.build_argv(short_dict=kwargs)
			cmd = self.run_manage_command_raw(YourManageCommandClass, argv=argv)
		"""
		command = cmd_class(stdout=StringIO(), stderr=StringIO())

		with mock.patch('django.core.management.base.connections.close_all'):
			# patch to prevent closing db connecction
			command.run_from_argv(argv)
		return command


#COMMON SYSTEM TESTS. Move model specific tests to file of its own

## TESTING !! REMOVE LATER
