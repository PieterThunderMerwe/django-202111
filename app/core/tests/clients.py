from core.tests import EnTestCase #exctends django.test.TestCase
from core.utils import *
import sys, warnings


#OnChange run:
"""
docker-compose run --rm app sh -c "python -Wa manage.py test"
docker-compose run --rm app sh -c "python -Wa manage.py test --keepdb"
"""




## TESTING !! REMOVE LATER
from core.models import Client
class ClientTestCase(EnTestCase):
	sig = "ClientTestCase"
	def setUp(self):
		# warnings.simplefilter('ignore', category=RuntimeWarning)  #ignore warnings - wip

		Client.objects.create(ClientName="CORRECT"
			, ClientCode="CN0001"
			, Logo="Logo"
			, ContactEmail="info@cn1.com"
			, ApiKey="1234567890ABCDEF1234567890ABCDEF"
			, ApiDeliveryEndpoint="http://api.cn1.com/ApiDeliveryEndpoint")
		Client.objects.create(ClientName="INCORRECT"
			, ClientCode="CN0002"
			, Logo="Logo"
			, ContactEmail="info@cn2.com"
			, ApiKey="ABC"
			, ApiDeliveryEndpoint="http://api.cn1.com/ApiDeliveryEndpoint")

	def test_apikey(self):
		# sys.stdout.write("\nLOADING "+ self.sig +"."+ get_functioname() +"() ("+ __name__ +")")

		CN1 = Client.objects.get(ClientCode="CN0001")
		self.assertIsInstance(CN1, Client)
		CN2 = Client.objects.get(ClientCode="CN0002")
		self.assertIsInstance(CN2, Client)

		self.assertEqual(CN1.getApiKeyLength(), Client.ApiKey_max_length)
		self.assertNotEqual(CN2.getApiKeyLength(), Client.ApiKey_max_length)

		# TestClients = (CN1, CN2)
		# for client in TestClients:
		# 	self.assertEqual(client.test_apikey(), Client.ApiKey_max_length)

	def test_apikey2(self):
		# sys.stdout.write("\nLOADING "+ self.sig +"."+ get_functioname() +"() ("+ __name__ +")")

		CN1 = Client.objects.get(ClientCode="CN0001")
		self.assertIsInstance(CN1, Client)
		CN2 = Client.objects.get(ClientCode="CN0002")
		self.assertIsInstance(CN2, Client)

		self.assertEqual(CN1.getApiKeyLength(), Client.ApiKey_max_length)
		self.assertNotEqual(CN2.getApiKeyLength(), Client.ApiKey_max_length)

		# TestClients = (CN1, CN2)
		# for client in TestClients:
		# 	self.assertEqual(client.test_apikey(), Client.ApiKey_max_length)