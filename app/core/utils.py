"""
Commonly used functions
"""

import secrets
def createApiKey(nbytes=24):
	return secrets.token_urlsafe(nbytes)
#https://stackoverflow.com/questions/976577/random-hash-in-python
# UobbniHEseMUsBryGIBxNA
#-2ZgLQRGBtJneREh65yGOaukDkpaOTDH

import traceback
def get_functioname():
	stack = traceback.extract_stack()
	filename, codeline, funcName, text = stack[-2]
	return funcName