
#Django
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include
import app as App
import datetime
import sys

#My Applications
#from core.views import test_view #testapi
#from core.views import TestView #restapi
from core.views import * #just get everyitng defined in core.views.py

#3rd Party
from rest_framework import routers, serializers, views, viewsets, schemas #restapi
#from rest_framework.schemas import get_schema_view

#MOVE TO CLASS


#Routes
router = routers.DefaultRouter()
router.register(r'api-auth/clients', ClientViewSet)
router.register(r'api-auth/languages', LanguageViewSet)
router.register(r'api-auth/user-language-ratings', UserLanguageRatingViewSet)
#router.register(r'my/path/nice-url-slug-please!', ObjectViewSet)


## TESTING !! REMOVE LATER
#router.register(r'users', UserViewSet)
#TEST: http://localhost:8000/users/ (see '' urlpatterns below)
router.register(r'api-auth/users', UserViewSet)
#TEST: http://localhost:8000/api-auth/users/ (see 'api-auth/' urlpatterns below)
router.register(r'api-auth/geeks', GeeksViewSet)


#urlpatterns
urlpatterns = [
	path('', include(router.urls)), #sys.stdout.write(str(router.urls))
	# TODO: change to home landing page path('', include(router.urls)), #sys.stdout.write(str(router.urls))
	#admin
	path('admin/', admin.site.urls),

	#api
	path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	#path('api/test', test_view, name='test'), #testapi
	path('api/test', TestView.as_view(), name='test'), #restapi
	path('api/schema', schemas.get_schema_view(title=App.__name__ ,description="OpenApi Schema - "+ str(datetime.datetime.now()),version=App.__build__,), name='openapi-schema'),
]

if settings.DEBUG:
	urlpatterns += static(
		settings.MEDIA_URL,
		document_root=settings.MEDIA_ROOT
	)
	urlpatterns += static(
		settings.STATIC_URL,
		document_root=settings.STATIC_ROOT
	)


"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.urls import include, path
	2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""