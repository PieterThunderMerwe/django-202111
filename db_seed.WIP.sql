--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5
-- Dumped by pg_dump version 13.3

-- Started on 2022-01-06 13:17:02

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 3104 (class 0 OID 16416)
-- Dependencies: 207
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.auth_group VALUES (1, '1-Project Admin');
INSERT INTO public.auth_group VALUES (2, '2-QA');
INSERT INTO public.auth_group VALUES (3, '3-Reviewer');
INSERT INTO public.auth_group VALUES (4, '4-Transcriber');
INSERT INTO public.auth_group VALUES (5, 'V-Client');


--
-- TOC entry 3106 (class 0 OID 16426)
-- Dependencies: 209
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.auth_group_permissions VALUES (1, 1, 33);
INSERT INTO public.auth_group_permissions VALUES (2, 1, 34);
INSERT INTO public.auth_group_permissions VALUES (3, 1, 35);
INSERT INTO public.auth_group_permissions VALUES (4, 1, 4);
INSERT INTO public.auth_group_permissions VALUES (5, 1, 36);
INSERT INTO public.auth_group_permissions VALUES (6, 1, 13);
INSERT INTO public.auth_group_permissions VALUES (7, 1, 14);
INSERT INTO public.auth_group_permissions VALUES (8, 1, 15);
INSERT INTO public.auth_group_permissions VALUES (9, 1, 16);
INSERT INTO public.auth_group_permissions VALUES (10, 2, 16);
INSERT INTO public.auth_group_permissions VALUES (11, 2, 4);
INSERT INTO public.auth_group_permissions VALUES (12, 2, 36);
INSERT INTO public.auth_group_permissions VALUES (13, 3, 16);
INSERT INTO public.auth_group_permissions VALUES (14, 3, 4);
INSERT INTO public.auth_group_permissions VALUES (15, 3, 36);


--
-- TOC entry 3102 (class 0 OID 16408)
-- Dependencies: 205
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.auth_permission VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO public.auth_permission VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO public.auth_permission VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO public.auth_permission VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO public.auth_permission VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO public.auth_permission VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO public.auth_permission VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO public.auth_permission VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO public.auth_permission VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO public.auth_permission VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO public.auth_permission VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO public.auth_permission VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO public.auth_permission VALUES (13, 'Can add user', 4, 'add_user');
INSERT INTO public.auth_permission VALUES (14, 'Can change user', 4, 'change_user');
INSERT INTO public.auth_permission VALUES (15, 'Can delete user', 4, 'delete_user');
INSERT INTO public.auth_permission VALUES (16, 'Can view user', 4, 'view_user');
INSERT INTO public.auth_permission VALUES (17, 'Can add content type', 5, 'add_contenttype');
INSERT INTO public.auth_permission VALUES (18, 'Can change content type', 5, 'change_contenttype');
INSERT INTO public.auth_permission VALUES (19, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO public.auth_permission VALUES (20, 'Can view content type', 5, 'view_contenttype');
INSERT INTO public.auth_permission VALUES (21, 'Can add session', 6, 'add_session');
INSERT INTO public.auth_permission VALUES (22, 'Can change session', 6, 'change_session');
INSERT INTO public.auth_permission VALUES (23, 'Can delete session', 6, 'delete_session');
INSERT INTO public.auth_permission VALUES (24, 'Can view session', 6, 'view_session');
INSERT INTO public.auth_permission VALUES (25, 'Can add sample', 7, 'add_sample');
INSERT INTO public.auth_permission VALUES (26, 'Can change sample', 7, 'change_sample');
INSERT INTO public.auth_permission VALUES (27, 'Can delete sample', 7, 'delete_sample');
INSERT INTO public.auth_permission VALUES (28, 'Can view sample', 7, 'view_sample');
INSERT INTO public.auth_permission VALUES (29, 'Can add geeks model', 8, 'add_geeksmodel');
INSERT INTO public.auth_permission VALUES (30, 'Can change geeks model', 8, 'change_geeksmodel');
INSERT INTO public.auth_permission VALUES (31, 'Can delete geeks model', 8, 'delete_geeksmodel');
INSERT INTO public.auth_permission VALUES (32, 'Can view geeks model', 8, 'view_geeksmodel');
INSERT INTO public.auth_permission VALUES (33, 'Can add client', 9, 'add_client');
INSERT INTO public.auth_permission VALUES (34, 'Can change client', 9, 'change_client');
INSERT INTO public.auth_permission VALUES (35, 'Can delete client', 9, 'delete_client');
INSERT INTO public.auth_permission VALUES (36, 'Can view client', 9, 'view_client');


--
-- TOC entry 3108 (class 0 OID 16434)
-- Dependencies: 211
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.auth_user VALUES (6, 'pbkdf2_sha256$260000$heHAJ7KHJiK92RE6dC4wUn$4ss3lHpm4PrjcDZxrXNeltaUY0bD2oV1r5KUZvi3dak=', NULL, false, 'client@enlabeler.com', 'Client', '', 'client@enlabeler.com', true, true, '2022-01-06 10:43:37+00');
INSERT INTO public.auth_user VALUES (3, 'pbkdf2_sha256$260000$BpxgxdJomiWvihQc5ve0E1$xO7jRpYmpCETegEyYvqQw1PMFUCVrLa0kNjicenQ418=', NULL, false, 'qa@enlabeler.com', 'QA', '', 'qa@enlabeler.com', true, true, '2022-01-06 10:42:49+00');
INSERT INTO public.auth_user VALUES (4, 'pbkdf2_sha256$260000$Vq2t2FFolIwHhaSvuAasmj$N92B9o5qxqPQt/xg0HtbPtNnCwUQ3vy5tMAepkk6iqA=', NULL, false, 'reviewer@enlabeler.com', 'Reviewer', '', 'reviewer@enlabeler.com', true, true, '2022-01-06 10:43:07+00');
INSERT INTO public.auth_user VALUES (5, 'pbkdf2_sha256$260000$NMI06vt9bUojOuAPGAwVBu$vEvn3nbyB4ZukzfaH7+4wXgimZirn/G13DTpbvEBtiQ=', '2022-01-06 10:50:53+00', false, 'transcriber@enlabeler.com', 'Transcriber', '', 'transcriber@enlabeler.com', true, true, '2022-01-06 10:43:22+00');
INSERT INTO public.auth_user VALUES (2, 'pbkdf2_sha256$260000$Vd8W8MjKyztrQ6roI9EL9y$LkInhTgZBL9kcJmIkiXcDfQpz1HQWxUj8ynPrmr7GHI=', '2022-01-06 10:54:52.794597+00', false, 'projectadmin@enlabeler.com', 'ProjectAdmin', '', 'projectadmin@enlabeler.com', true, true, '2022-01-06 10:42:30+00');
INSERT INTO public.auth_user VALUES (1, 'pbkdf2_sha256$260000$c4MRtz9opuTtYBzINWjNjJ$IS3HnsQrmgwfOEzhOK87RRrkwlQVeTC5Sk9ej5+e+nM=', '2022-01-06 10:55:02.278168+00', true, 'admin', '', '', 'admin@enlabeler.com', true, true, '2022-01-05 18:34:28.431853+00');


--
-- TOC entry 3110 (class 0 OID 16444)
-- Dependencies: 213
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.auth_user_groups VALUES (1, 6, 5);
INSERT INTO public.auth_user_groups VALUES (2, 2, 1);
INSERT INTO public.auth_user_groups VALUES (3, 3, 2);
INSERT INTO public.auth_user_groups VALUES (4, 4, 3);
INSERT INTO public.auth_user_groups VALUES (5, 5, 4);


--
-- TOC entry 3112 (class 0 OID 16452)
-- Dependencies: 215
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: devuser
--



--
-- TOC entry 3114 (class 0 OID 16569)
-- Dependencies: 223
-- Data for Name: core_client; Type: TABLE DATA; Schema: public; Owner: devuser
--

INSERT INTO public.core_client VALUES (1, 'EnLabeler', '1234567890ABCDEF1234567890ABCDEF', NULL, true, 'en-icon-large_a6RWrCL.png', 'EN0001', '2022-01-05 19:14:12.451985+00', '2022-01-05 19:18:57.146142+00', '', 'info@enlabeler.com');


--
-- TOC entry 3127 (class 0 OID 0)
-- Dependencies: 206
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 5, true);


--
-- TOC entry 3128 (class 0 OID 0)
-- Dependencies: 208
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 15, true);


--
-- TOC entry 3129 (class 0 OID 0)
-- Dependencies: 204
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 36, true);


--
-- TOC entry 3130 (class 0 OID 0)
-- Dependencies: 212
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 5, true);


--
-- TOC entry 3131 (class 0 OID 0)
-- Dependencies: 210
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 6, true);


--
-- TOC entry 3132 (class 0 OID 0)
-- Dependencies: 214
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 3133 (class 0 OID 0)
-- Dependencies: 222
-- Name: core_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: devuser
--

SELECT pg_catalog.setval('public.core_client_id_seq', 1, true);

-- Completed on 2022-01-06 13:17:02

--
-- PostgreSQL database dump complete
--

